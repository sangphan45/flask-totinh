from flask import Flask, render_template, request
from flask_pymongo import PyMongo
import datetime
import requests
import json

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb+srv://sangphan45:sangphan45@cluster0.zbti8u5.mongodb.net/test"
mongodb_client = PyMongo(app)

db = mongodb_client.db

accessTime = datetime.datetime.now()

@app.route('/')
def index():
   global accessTime
   if request.headers.get('User-Agent') != "Typhoeus - https://github.com/typhoeus/typhoeus":
      accessTime = datetime.datetime.now() + datetime.timedelta(hours = 7)
      
      if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
        publicIP = request.environ['REMOTE_ADDR']
      else:
         publicIP = request.environ['HTTP_X_FORWARDED_FOR']
         
      location = requests.get("https://ipgeolocation.abstractapi.com/v1/?api_key=5540af6739cf4febb1e844145f6914a0&ip_address={0}".format(publicIP))
      db.log.insert_one(
         {
            'accessTime': accessTime,
            'publicIP': publicIP,
            'location': json.loads(location.text),
            'privateIP': request.environ.get('HTTP_X_REAL_IP', request.remote_addr),
            'userAgent': request.headers.get('User-Agent')
         }
      )
   return render_template('index.html')

@app.route('/heart')
def heart():
   global accessTime
   db.log.update_one(
      {
         'accessTime': accessTime
      },
      {
         "$set": { 'heartPage': True }
      }
   )
   return render_template('heart.html')

if __name__ == '__main__':
   app.run()